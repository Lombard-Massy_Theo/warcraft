﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Unite;

namespace warcrafttest
{
    [TestClass]
    public class warcraftTest
    {
        [TestMethod]
        public void TestPeonConstructor()
        {
           Peon peon = new Peon("Peon Bolgar", "orc", "la horde", 30, 0);

            Assert.AreEqual("Peon Bolgar", peon.name);
            Assert.AreEqual("orc", peon.race);
            Assert.AreEqual("la horde", peon.faction);
            Assert.AreEqual(30, peon.hit_point);
            Assert.AreEqual(0, peon.armor);
        }

        [TestMethod]
        public void TestPeonGrunt()
        {
            Peon peon = new Peon("Peon Bolgar", "orc", "la horde", 30, 0);
            Assert.AreEqual("Grr", peon.Peongrunt());
        }

        [TestMethod]
        public void TestPeonSayHello()
        {
            Peon peon = new Peon("Peon Bolgar", "orc", "la horde", 30, 0);
            Assert.AreEqual("Je m'apelle Peon Bolgar, je suis un orc de la horde.", peon.PeonsayHello());
        }

        [TestMethod]
        public void TestPeonTalk()
        {
            Peon peon = new Peon("Peon Bolgar", "orc", "la horde", 30, 0);
            Assert.AreEqual("Work Work", peon.Peontalk());
        }

        [TestMethod]
        public void TestPeonTalkToPeon()
        {
            Peon peon = new Peon("Peon Bolgar", "orc", "la horde", 30, 0);
            Assert.AreEqual("Longue vie a la horde", peon.Peontalktopeon());
        }

        [TestMethod]
        public void TestPeonTalkToPeasant()
        {
            Peon peon = new Peon("Peon Bolgar", "orc", "la horde", 30, 0);
            Assert.AreEqual("Je te defis en duel", peon.Peontalktopeasant());
        }
        [TestMethod]
        public void TestPeasantConstructor()
        {
            Peasant Peasant = new Peasant("xavier", "humain", "alliance", 30, 0);

            Assert.AreEqual("xavier", Peasant.name);
            Assert.AreEqual("humain", Peasant.race);
            Assert.AreEqual("alliance", Peasant.faction);
            Assert.AreEqual(30, Peasant.hit_point);
            Assert.AreEqual(0, Peasant.armor);
        }

        [TestMethod]
        public void TestPeasantGrunt()
        {
            Peasant Peasant = new Peasant("xavier", "humain", "l'alliance", 30, 0);
            Assert.AreEqual("Mmh", Peasant.Peasantgrunt());
        }

        [TestMethod]
        public void TestPeasantSayHello()
        {
            Peasant Peasant = new Peasant("xavier", "humain", "l'alliance", 30, 0);
            Assert.AreEqual("Je m'apelle xavier, je suis un humain de l'alliance.", Peasant.PeasantsayHello());
        }

        [TestMethod]
        public void TestPeasantTalk()
        {
            Peasant Peasant = new Peasant("xavier", "humain", "l'alliance", 30, 0);
            Assert.AreEqual("C'est une bien belle beauté, j'ai bien envie de la défleurer a l'arriere de mon pré!", Peasant.Peasanttalk());
        }

        [TestMethod]
        public void TestPeasantTalkToPeasant()
        {
            Peasant Peasant = new Peasant("xavier", "humain", "l'alliance", 30, 0);
            Assert.AreEqual("Allons à la taverne camarade", Peasant.Peasanttalktopeasant());
        }

        [TestMethod]
        public void TestPeasantTalkToPeon()
        {
            Peasant Peasant = new Peasant("xavier", "humain", "l'alliance", 30, 0);
            Assert.AreEqual("Je tuerai ta race!", Peasant.Peasanttalktopeon());
        }
    }
}
