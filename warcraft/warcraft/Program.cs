﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace warcraft 
{
    class Program
    {
        static string setName()
        {
            Console.Write("Donnez lui un nom : ");
            string name = Console.ReadLine();
            return name;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Créer un Peasant :");
            string name = setName();
            string race_peasant = "Humain";
            string faction_peasant = "L'Alliance";
            int hit_point_peasant = 30;
            int armor_peasant = 0;
            Unite.Peasant peasant = new Unite.Peasant(name, race_peasant, faction_peasant, hit_point_peasant, armor_peasant);

            Console.WriteLine("Créer un Peon :");
            name = setName();
            string race_peon = "Orc";
            string faction_peon = "La Horde";
            int hit_point_peon = 30;
            int armor_peon = 0;
            Unite.Peon peon = new Unite.Peon(name, race_peon, faction_peon, hit_point_peon, armor_peon);

            Console.WriteLine(peasant.PeasantsayHello());
            Console.WriteLine(peon.PeonsayHello());
            Console.WriteLine(peon.Peongrunt());
            Console.WriteLine(peasant.Peasanttalk());
            Console.WriteLine(peon.Peontalk());
            Console.WriteLine(peasant.Peasantgrunt());
            Console.WriteLine(peasant.Peasanttalktopeasant());
            Console.WriteLine(peasant.Peasanttalktopeon());
            Console.WriteLine(peon.Peontalktopeasant());
            Console.WriteLine(peon.Peontalktopeon());
            Console.ReadKey();
        }
    }
}
