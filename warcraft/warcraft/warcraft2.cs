﻿using System;

namespace Unite
{
    public class Peon
    {
        public string name;
        public string race;
        public string faction;
        public int hit_point;
        public int armor;

        public Peon(string name, string race, string faction, int hit_point, int armor)
        {
            this.name = name;
            this.race = race;
            this.faction = faction;
            this.hit_point = hit_point;
            this.armor = armor;
        }
        public string PeonsayHello()
        {
            return string.Format("Je m'apelle {0}, je suis un {1} de {2}.", this.name, this.race, this.faction);
        }
        public string Peontalk()
        {
            return "Work Work";
        }
        public string Peongrunt()
        {
            return "Grr";
        }
        public string Peontalktopeon()
        {
            return "Longue vie a la horde";
        }
        public string Peontalktopeasant()
        {
            return "Je te defis en duel";
        }
    }
    public class Peasant
    {
        public string name;
        public string race;
        public string faction;
        public int hit_point;
        public int armor;

        public Peasant(string name, string race, string faction, int hit_point, int armor)
        {
            this.name = name;
            this.race = race;
            this.faction = faction;
            this.hit_point = hit_point;
            this.armor = armor;
        }
        public string PeasantsayHello()
        {
            return string.Format("Je m'apelle {0}, je suis un {1} de {2}.", this.name, this.race, this.faction);
        }
        public string Peasanttalk()
        {
            return "C'est une bien belle beauté, j'ai bien envie de la défleurer a l'arriere de mon pré!";
        }
        public string Peasantgrunt()
        {
            return "Mmh";
        }
        public string Peasanttalktopeon()
        {
            return "Je tuerai ta race!";
        }
        public string Peasanttalktopeasant()
        {
            return "Allons à la taverne camarade";
        }
    }
}